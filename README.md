#Time Range List Subtraction

To solve the time range list subtraction problem, I created two simple objects representing time and time range.
The Time object is very simple and only supports hours and minutes, however it could easily be replaced with a library DateTime object.

The Time object is comparable, allowing for comparisons between the ranges and their start/end times.

The trivial approach would be to compare every time range from the first list to every time range in the second.
The time complexity would be n^2.
In order to solve this problem in better than n^2 time, I've decided to use an Interval Tree.
I used a library implementation of the tree and implemented the Time and TimeRange Objects as well as logic to shorten and split conflicting time ranges.

The program accepts a csv file as input. Every row of the input file represents a list of time ranges.
Every second list in the file is subtracted from the previous list defined on the row above it.

I've included a sample input file: timeRanges.csv
The format of the time ranges is `HH:mm-HH:mm`, eg. `9:00-9:45`

The project dependencies are managed via Maven.
To compile and run the program, execute the following from the root of the project:

`mvn compile exec:java -Dexec.args="timeRanges.csv"`

I have also included several tests with my code.