package com.league.challenge;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RangeListSubtractTest {
	
	/**
	 * Tests subtracting same range from itself
	 */
	@Test
	void testSubtractSameRange() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(0, result.size());
	}
	
	/**
	 * Tests subtracting range with same start time and earlier end time
	 */
	@Test
	void testSubtractRangeWithSameStartEndingEarlier() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 0),
			new Time(4, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		TimeRange expected = new TimeRange(
			new Time(4, 0),
			new Time(5, 0)
		);
		
		assertEquals(1, result.size());
		assertEquals(expected, result.get(0));
	}
	
	/**
	 * Tests subtracting range with same start time and later end time
	 */
	@Test
	void testSubtractRangeWithSameStartEndingLater() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 0),
			new Time(10, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(0, result.size());
	}
	
	/**
	 * Tests subtracting range with same end time and earlier start time
	 */
	@Test
	void testSubtractRangeWithSameEndStartingEarlier() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(2, 0),
			new Time(5, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(0, result.size());
	}
	
	/**
	 * Tests subtracting range with same start time and later end time
	 */
	@Test
	void testSubtractRangeWithSameEndStartingLater() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(3, 0),
			new Time(5, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		TimeRange expected = new TimeRange(
			new Time(1, 0),
			new Time(3, 0)
		);
		
		assertEquals(1, result.size());
		assertEquals(expected, result.get(0));
	}
	
	/**
	 * Tests subtracting a range overlapping start time
	 */
	@Test
	void testSubtractOverlappingStart() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(10, 0),
			new Time(11, 0)
		));
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(9, 15),
			new Time(10, 15)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		TimeRange expected = new TimeRange(
			new Time(10, 15),
			new Time(11, 0)
		);
		
		assertEquals(1, result.size());
		assertEquals(expected, result.get(0));
	}
	
	/**
	 * Tests subtracting a range overlapping end time
	 */
	@Test
	void testSubtractOverlappingEnd() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(9, 15),
			new Time(10, 15)
		));
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(10, 0),
			new Time(11, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		TimeRange expected = new TimeRange(
			new Time(9, 15),
			new Time(10, 0)
		);
		
		assertEquals(1, result.size());
		assertEquals(expected, result.get(0));
	}
	
	/**
	 * Tests subtracting a contained range
	 */
	@Test
	void testSubtractContainedRange() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(10, 0)
		));
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(4, 0),
			new Time(5, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		List<TimeRange> expected = new LinkedList<>();
		expected.add(new TimeRange(
			new Time(1, 0),
			new Time(4, 0)
		));
		expected.add(new TimeRange(
			new Time(5, 0),
			new Time(10, 0)
		));
		
		assertEquals(2, result.size());
		assertEquals(expected, result);
	}
	
	/**
	 * Tests subtracting empty range (start == end)
	 */
	@Test
	void testSubtractContainedEmptyRange() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(10, 0)
		));
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(4, 0),
			new Time(4, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		List<TimeRange> expected = new LinkedList<>();
		expected.add(new TimeRange(
			new Time(1, 0),
			new Time(10, 0)
		));
		
		assertEquals(1, result.size());
		assertEquals(expected, result);
	}
	
	/**
	 * Tests subtracting a containing range
	 */
	@Test
	void testSubtractContainingRange() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(4, 0),
			new Time(5, 0)
		));
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 0),
			new Time(10, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(0, result.size());
	}
	
	/**
	 * Tests subtracting range overlapping multiple ranges in left list
	 */
	@Test
	void testSubtractRangeOverlappingMultiple() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(2, 0)
		));
		left.add(new TimeRange(
			new Time(3, 0),
			new Time(4, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 30),
			new Time(3, 30)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		List<TimeRange> expected = new LinkedList<>();
		expected.add(new TimeRange(
			new Time(1, 0),
			new Time(1, 30)
		));
		expected.add(new TimeRange(
			new Time(3, 30),
			new Time(4, 0)
		));
		
		assertEquals(2, result.size());
		assertEquals(expected, result);
	}
	
	/**
	 * Tests subtracting multiple ranges overlapping a single range
	 */
	@Test
	void testSubtractMultipleOverlapsSingleRange() {
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		));
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(1, 0),
			new Time(2, 0)
		));
		right.add(new TimeRange(
			new Time(2, 30),
			new Time(3, 0)
		));
		right.add(new TimeRange(
			new Time(4, 15),
			new Time(6, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		List<TimeRange> expected = new LinkedList<>();
		expected.add(new TimeRange(
			new Time(2, 0),
			new Time(2, 30)
		));
		expected.add(new TimeRange(
			new Time(3, 0),
			new Time(4, 15)
		));
		
		assertEquals(2, result.size());
		assertEquals(expected, result);
	}
	
	/**
	 * Tests subtracting a non overlapping range
	 */
	@Test
	void testSubtractNoOverlap() {
		TimeRange range = new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		);
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(range);
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(6, 0),
			new Time(7, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(1, result.size());
		assertEquals(range, result.get(0));
	}
	
	/**
	 * Tests subtracting a reversed range (end < start)
	 */
	@Test
	void testSubtractReversedRange() {
		TimeRange range = new TimeRange(
			new Time(1, 0),
			new Time(5, 0)
		);
		LinkedList<TimeRange> left = new LinkedList<>();
		left.add(range);
		
		LinkedList<TimeRange> right = new LinkedList<>();
		right.add(new TimeRange(
			new Time(5, 0),
			new Time(1, 0)
		));
		
		RangeList rangeList = new RangeList(left);
		rangeList.subtract(right);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(1, result.size());
		assertEquals(range, result.get(0));
	}
	
}