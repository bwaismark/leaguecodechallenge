package com.league.challenge;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TimeRangeTest {
	
	@Test
	void getMidpointSimpleTest() {
		TimeRange range = new TimeRange(
			new Time(9, 0),
			new Time(10, 0)
		);
		
		Time expected = new Time(9,30);
		Assertions.assertEquals(expected, range.getMidpoint());
	}
	
}