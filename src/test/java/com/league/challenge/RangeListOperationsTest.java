package com.league.challenge;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RangeListOperationsTest {
	
	@Test
	void testAsList() {
		LinkedList<TimeRange> expected = new LinkedList<>();
		expected.add(new TimeRange(
			new Time(1, 0),
			new Time(2, 0)
		));
		expected.add(new TimeRange(
			new Time(3, 0),
			new Time(4, 0)
		));
		expected.add(new TimeRange(
			new Time(5, 0),
			new Time(6, 0)
		));
		
		RangeList rangeList = new RangeList(expected);
		List<TimeRange> result = rangeList.asList();
		
		assertEquals(3, result.size());
		assertEquals(expected, result);
	}
	
	@Test
	void testAddRamge() {
		TimeRange range = new TimeRange(
			new Time(0, 0),
			new Time(23, 59)
		);
		
		RangeList rangeList = new RangeList();
		rangeList.addTimeRange(range);
		
		List<TimeRange> result = rangeList.asList();
		assertEquals(1, result.size());
		assertEquals(range, result.get(0));
	}
	
	@Test
	void testRamoveRange() {
		List<TimeRange> list = new LinkedList<>();
		TimeRange range = new TimeRange(
			new Time(0, 0),
			new Time(23, 59)
		);
		list.add(range);
		
		RangeList rangeList = new RangeList(list);
		rangeList.removeTimeRange(range);
		
		List<TimeRange> result = rangeList.asList();
		assertEquals(0, result.size());
	}
	
}