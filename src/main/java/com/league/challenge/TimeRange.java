package com.league.challenge;

import com.lodborg.intervaltree.Interval;
import org.apache.log4j.Logger;

import java.io.IOException;

public class TimeRange extends Interval<Time> {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	// Used for reflection
	private TimeRange() {
		super();
	}
	
	public TimeRange(Time start, Time end) {
		super(start, end, Bounded.OPEN);
		
		if (start.compareTo(end) < 0) {
			logger.warn("Range start " + start.toString() + " is greater than range end " + end.toString());
		}
	}
	
	protected Interval<Time> create() {
		return new TimeRange();
	}
	
	public Time getMidpoint() {
		int totalDifference = getEnd().getMinuteOfDay() - getStart().getMinuteOfDay();
		int hourDifference = (totalDifference / 2) / 60;
		int minuteDifference = (totalDifference / 2) % 60;
		
		int hour = getStart().getHourOfDay() + hourDifference;
		int minute = getStart().getMinuteOfHour() + minuteDifference;
		
		if (minute > 59) {
			hour += 1;
			minute -= 60;
		}
		
		return new Time(
			hour,
			minute
		);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TimeRange) {
			TimeRange other = (TimeRange) obj;
			return this.getStart().equals(other.getStart())
				&& this.getEnd().equals(other.getEnd());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getStart().toString() + "-" + getEnd().toString();
	}
	
	/**
	 * parse String to TimeRange
	 * @param range
	 * @return
	 * @throws IOException
	 */
	public static TimeRange parse(String range) throws IOException {
		String[] times = range.split("-", 2);
		
		if (times.length != 2) {
			throwFormatException();
		}
		
		String[] startString = times[0].trim().split(":");
		if (startString.length != 2) {
			throwFormatException();
		}
		Time startTime = new Time(Integer.parseInt(startString[0]), Integer.parseInt(startString[1]));
		
		String[] endString = times[1].trim().split(":");
		if (endString.length != 2) {
			throwFormatException();
		}
		Time endTime = new Time(Integer.parseInt(endString[0]), Integer.parseInt(endString[1]));
		
		return new TimeRange(startTime, endTime);
	}
	
	private static void throwFormatException() throws IOException {
		throw new IOException("Incorrect input time range format. Please use 'HH:mm-HH:mm'");
	}
	
}
