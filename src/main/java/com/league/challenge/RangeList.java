package com.league.challenge;

import com.lodborg.intervaltree.Interval;
import com.lodborg.intervaltree.IntervalTree;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RangeList {
	
	private final IntervalTree<Time> leftTree;
	
	RangeList() {
		leftTree = new IntervalTree<>();
	}
	
	/**
	 * Creates a RangeList containing all input elements
	 * @param initial - initial list of TimeRanges in this RangeList
	 */
	RangeList(List<TimeRange> initial) {
		// Create an IntervalTree from left operand
		leftTree = new IntervalTree<>();
		leftTree.addAll(initial);
	}
	
	/**
	 * Subtracts a list of {@link TimeRange} objects from this RangeList
	 * @param right - a list of {@link TimeRange} to be removed
	 */
	public void subtract(List<TimeRange> right) {
		// remove all ranges in left intersecting with ranges from right
		for (TimeRange toRemove : right) {
			removeTimeRange(toRemove);
		}
	}
	
	/**
	 * Adds a range to this RangeList from specified start and end {@link Time} objects
	 * @param start
	 * @param end
	 */
	public void addTimeRange(Time start, Time end) {
		// ignore empty ranges (start == end)
		if (start.compareTo(end) != 0) {
			addTimeRange(new TimeRange(start, end));
		}
	}
	
	/**
	 * Adds a range to this RangeList
	 * @param range - {@link TimeRange} to be added
	 */
	public void addTimeRange(TimeRange range) {
		leftTree.add(range);
	}
	
	/**
	 * Removed a {@link TimeRange} from any overlapping {@link TimeRange} contained in this RangeList
	 * @param toRemove - {@link TimeRange} to be removed
	 */
	public void removeTimeRange(TimeRange toRemove) {
		// ignore empty range
		if (toRemove.isEmpty()) {
			return;
		}
		Set<Interval<Time>> intersections = leftTree.query(toRemove);
		for (Interval<Time> intersection : intersections) {
			// remove overlapping range
			leftTree.remove(intersection);
			
			if (intersection.contains(toRemove)) {
				// split to two ranges
				addTimeRange(intersection.getStart(), toRemove.getStart());
				addTimeRange(toRemove.getEnd(), intersection.getEnd());
			} else if (intersection.getStart().compareTo(toRemove.getStart()) < 0) {
				// shorten end
				addTimeRange(intersection.getStart(), toRemove.getStart());
			} else {
				// advance start
				addTimeRange(toRemove.getEnd(), intersection.getEnd());
			}
		}
	}
	
	/**
	 * Returns a list representation of a RangeList object
	 * @return - a List of {@link TimeRange} objects
	 */
	public List<TimeRange> asList() {
		// Regenerate list from tree, sorted by start time
		return leftTree.stream()
			.map(range -> (TimeRange) range)
			.sorted((a, b) -> a.getStart().compareTo(b.getStart()))
			.collect(Collectors.toList());
	}
	
}
