package com.league.challenge;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class RangeListSubtractor {
	
	/**
	 * This main method reads a file containing lists of time ranges.
	 * Every odd list will be subtracted from the list specified above it and the results will be printed
	 *
	 * @param args - accepts a single argument, path to file containing lists of time ranges
	 */
	public static void main(String[] args) {
		String path = args[0];
		List<List<TimeRange>> timeRanges = readTimeRangesFromFile(path);
		while (timeRanges.size() > 1) {
			List<TimeRange> left = timeRanges.remove(0);
			List<TimeRange> right = timeRanges.remove(0);
			
			RangeList rangeList = new RangeList(left);
			rangeList.subtract(right);
			printTimeRangeLIst(rangeList.asList());
		}
		
		// If there are an odd number of lines in input file, last line is printed unchanged
		if (!timeRanges.isEmpty()) {
			printTimeRangeLIst(timeRanges.get(0));
		}
	}
	
	private static void printTimeRangeLIst(List<TimeRange> ranges) {
		System.out.println(ranges.stream().map(TimeRange::toString).collect(Collectors.joining(", ")));
	}
	
	/**
	 * Reads lists of time ranges from specified file location
	 *
	 * @param path
	 * @return
	 */
	private static List<List<TimeRange>> readTimeRangesFromFile(String path) {
		List<List<TimeRange>> timeRangeLists = new LinkedList<>();
		
		try {
			FileReader fileReader = new FileReader(path);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				String[] ranges = line.split(",");
				List<TimeRange> timeRanges = new LinkedList<>();
				for (String range : ranges) {
					TimeRange timeRange = TimeRange.parse(range);
					timeRanges.add(timeRange);
				}
				timeRangeLists.add(timeRanges);
			}
			
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			System.err.println("Could not find file '" + path + "'");
		} catch (IOException e) {
			System.err.println("There was a problem reading from input file");
			System.err.println(e.getMessage());
		}
		
		return timeRangeLists;
	}
	
}
