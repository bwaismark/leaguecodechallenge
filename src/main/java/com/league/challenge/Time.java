package com.league.challenge;

import org.apache.log4j.Logger;

public class Time implements Comparable<Time> {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	private final int hourOfDay;
	private final int minuteOfHour;
	
	public Time(int hour, int minute) {
		if (hour < 0 || hour > 23) {
			logger.warn("Hour " + hour + " is outside allowed range [0-23].");
			if (hour < 0) {
				logger.warn("Hour " + hour + " will be set to 0.");
				hour = 0;
			} else {
				logger.warn("Hour " + hour + " will be set to 23.");
				hour = 23;
			}
		}
		if (minute < 0 || minute > 59) {
			logger.warn("Minute " + minute + " is outside allowed range [0-59].");
			if (minute < 0) {
				logger.warn("Minute " + minute + " will be set to 0.");
				minute = 0;
			} else {
				logger.warn("Minute " + minute + " will be set to 59.");
				minute = 59;
			}
		}
		
		this.hourOfDay = hour;
		this.minuteOfHour = minute;
	}
	
	public int getHourOfDay() {
		return hourOfDay;
	}
	
	public int getMinuteOfHour() {
		return minuteOfHour;
	}
	
	public int getMinuteOfDay() {
		return (this.hourOfDay * 60) + this.minuteOfHour;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Time) {
			Time other = (Time) obj;
			return this.hourOfDay == other.hourOfDay &&
				this.minuteOfHour == other.minuteOfHour;
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return hourOfDay + ":" + (minuteOfHour == 0 ? "00" : minuteOfHour);
	}
	
	/**
	 * Compares minutes of this time object to another
	 *
	 * @param o - other Time object to compare
	 * @return
	 */
	public int compareTo(Time o) {
		return this.getMinuteOfDay() - o.getMinuteOfDay();
	}
	
}
